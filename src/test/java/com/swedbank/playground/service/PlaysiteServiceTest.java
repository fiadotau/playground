package com.swedbank.playground.service;

import com.swedbank.playground.domain.impl.Visit;
import com.swedbank.playground.domain.impl.filter.VisitFilter;
import com.swedbank.playground.domain.impl.kid.Kid;
import com.swedbank.playground.domain.impl.kid.KidType;
import com.swedbank.playground.domain.impl.playsite.Playsite;
import com.swedbank.playground.domain.impl.playsite.PlaysiteType;
import com.swedbank.playground.repository.IVisitRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class PlaysiteServiceTest
{
	@Mock
	private IVisitRepository visitRepository;

	@InjectMocks
	private PlaysiteService playsiteService;

	@InjectMocks
	private Playsite playsite;

	@Before
	public void setUp() throws Exception
	{
		playsite.setPlaysiteType(PlaysiteType.DOUBLE_SWINGS);
	}

	@Test
	public void getPlaysiteUtilization()
	{
		Kid kid = new Kid("John Smith", 5, KidType.GENERAL);
		playsite.addKid(kid);

		assertEquals(0.5, playsiteService.getPlaysiteUtilization(playsite));
	}

	@Test
	public void getSpentTime()
	{
		Kid kid = new Kid("John Smith", 5, KidType.GENERAL);

		assertEquals(0, playsiteService.getSpentTime(playsite,kid));

		var startVisitFilter = VisitFilter.builder()
				.playsite(playsite)
				.kid(kid)
				.isStart(true)
				.build();

		var finishVisitFilter = VisitFilter.builder()
				.playsite(playsite)
				.kid(kid)
				.isStart(false)
				.build();

		var startVisit = new Visit();
		startVisit.setStart(true);
		startVisit.setPlaysite(playsite);
		startVisit.setKid(kid);
		startVisit.setTime(new Date());

		var finishVisit = new Visit();
		finishVisit.setStart(true);
		finishVisit.setPlaysite(playsite);
		finishVisit.setKid(kid);
		finishVisit.setTime(new Date(new Date().getTime() + 10000));

		Mockito.when(visitRepository.getByFilter(startVisitFilter)).thenReturn(List.of(startVisit));
		Mockito.when(visitRepository.getByFilter(finishVisitFilter)).thenReturn(List.of(finishVisit));

		long startDate = visitRepository.getByFilter(startVisitFilter).stream()
				.map(Visit::getTime)
				.mapToLong(Date::getTime)
				.sum();

		long finishDate = visitRepository.getByFilter(finishVisitFilter).stream()
				.map(Visit::getTime)
				.mapToLong(Date::getTime)
				.sum();

		assertEquals(10000, Math.abs(finishDate - startDate));
	}
}