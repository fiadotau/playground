package com.swedbank.playground.domain.impl.playsite;

import com.google.common.collect.ImmutableList;
import com.swedbank.playground.domain.impl.Visit;
import com.swedbank.playground.domain.impl.kid.Kid;
import com.swedbank.playground.domain.impl.kid.KidType;
import com.swedbank.playground.repository.IVisitRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class PlaysiteTest
{

	@Mock
	private IVisitRepository visitRepository;

	@InjectMocks
	private Playsite playsite;

	@Before
	public void setup()
	{
		playsite.setPlaysiteType(PlaysiteType.SLIDE);
		playsite.setMaxKidsPlaying(1);
	}

	@Test
	public void testAddKid()
	{
		Kid kid = new Kid("John Smith", 5, KidType.GENERAL);

		boolean playing = playsite.addKid(kid);
		assertEquals(true, playing);
		assertEquals(ImmutableList.of(kid), playsite.getKidsPlaying());

		final Visit visit = new Visit();
		visit.setPlaysite(playsite);
		visit.setKid(kid);
		visit.setStart(true);

		Mockito.verify(visitRepository).save(argThat(new ArgumentMatcher<>()
		{
			@Override
			public boolean matches(Object argument)
			{
				Visit v = (Visit) argument;
				return visit.getPlaysite().equals(v.getPlaysite())
						&& visit.getKid().equals(v.getKid())
						&& visit.isStart() == v.isStart();
			}
		}));

		Kid kid2 = new Kid("Johnus Smithus", 6, KidType.GENERAL);

		playing = playsite.addKid(kid2);
		assertEquals(false, playing);
		assertEquals(ImmutableList.of(kid2), playsite.getKidsWaiting());

		Mockito.verify(visitRepository, times(1)).save(any(Visit.class));

		Kid kid3 = new Kid("Yauheni Fiadotau", 25, KidType.VIP);
		Kid kid4 = new Kid("Ivan Ivanov", 15, KidType.GENERAL);
		Kid kid5 = new Kid("X Y", 16, KidType.GENERAL);
		Kid kid6 = new Kid("P V", 17, KidType.VIP);

		playsite.addKid(kid3);
		playsite.addKid(kid4);
		playsite.addKid(kid5);
		playsite.addKid(kid6);

		playsite.removeKid(kid);
		assertEquals(ImmutableList.of(kid3), playsite.getKidsPlaying());
		playsite.removeKid(kid3);
		assertEquals(ImmutableList.of(kid2), playsite.getKidsPlaying());
		playsite.removeKid(kid2);
		assertEquals(ImmutableList.of(kid4), playsite.getKidsPlaying());
		playsite.removeKid(kid4);
		assertEquals(ImmutableList.of(kid5), playsite.getKidsPlaying());
		playsite.removeKid(kid5);
		assertEquals(ImmutableList.of(kid6), playsite.getKidsPlaying());
	}

	@Test
	public void testRemoveKid()
	{
		Kid kid = new Kid("John Smith", 5, KidType.VIP);

		boolean playing = playsite.addKid(kid);
		assertEquals(true, playing);
		assertEquals(ImmutableList.of(kid), playsite.getKidsPlaying());

		playsite.removeKid(kid);
		assertEquals(ImmutableList.of(), playsite.getKidsPlaying());

		final Visit visit = new Visit();
		visit.setPlaysite(playsite);
		visit.setKid(kid);
		visit.setStart(false);

		Mockito.verify(visitRepository).save(argThat(new ArgumentMatcher<>()
		{
			@Override
			public boolean matches(Object argument)
			{
				Visit v = (Visit) argument;
				return visit.getPlaysite().equals(v.getPlaysite())
						&& visit.getKid().equals(v.getKid())
						&& visit.isStart() == v.isStart();
			}
		}));

		playsite.addKid(kid);


		Kid kid2 = new Kid("Johnus Smithus", 6, KidType.GENERAL);

		playsite.addKid(kid2);

		playsite.removeKid(kid2);
		assertEquals(ImmutableList.of(), playsite.getKidsWaiting());

		playsite.addKid(kid2);
		playsite.removeKid(kid);

		assertEquals(ImmutableList.of(kid2), playsite.getKidsPlaying());
	}

}
