package com.swedbank.playground.service;

import com.swedbank.playground.domain.IPlaysite;
import com.swedbank.playground.domain.impl.Visit;
import com.swedbank.playground.domain.impl.filter.VisitFilter;
import com.swedbank.playground.domain.impl.kid.Kid;
import com.swedbank.playground.repository.IVisitRepository;

import java.util.Date;

public class PlaysiteService
{
	private final IVisitRepository visitRepository;

	public PlaysiteService(IVisitRepository visitRepository)
	{
		this.visitRepository = visitRepository;
	}

	public double getPlaysiteUtilization(IPlaysite playsite)
	{
		return (double) playsite.getKidsPlaying().size() / playsite.getMaxKidsPlaying();
	}

	public long getSpentTime(IPlaysite playsite, Kid kid)
	{
		var startVisitFilter = VisitFilter.builder()
				.playsite(playsite)
				.kid(kid)
				.isStart(true)
				.build();

		var finishVisitFilter = VisitFilter.builder()
				.playsite(playsite)
				.kid(kid)
				.isStart(false)
				.build();

		long startDate = visitRepository.getByFilter(startVisitFilter).stream()
				.map(Visit::getTime)
				.mapToLong(Date::getTime)
				.sum();

		long finishDate = visitRepository.getByFilter(finishVisitFilter).stream()
				.map(Visit::getTime)
				.mapToLong(Date::getTime)
				.sum();

		return Math.abs(finishDate - startDate);
	}

}
