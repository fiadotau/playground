package com.swedbank.playground.domain.impl.kid;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Kid
{
	private String name;
	private int age;
	private KidType kidType;
}
