package com.swedbank.playground.domain.impl.playsite;

import com.google.common.collect.ImmutableList;
import com.swedbank.playground.domain.IPlaysite;
import com.swedbank.playground.domain.impl.Visit;
import com.swedbank.playground.domain.impl.kid.Kid;
import com.swedbank.playground.repository.IVisitRepository;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class Playsite implements IPlaysite
{
	@Getter
	private PlaysiteType playsiteType;

	private final List<Kid> kidsPlaying = new LinkedList<>();
	private final List<Kid> vipKidsWaiting = new LinkedList<>();
	private final List<Kid> kidsWaiting = new LinkedList<>();

	@Setter
	@Getter
	private int maxKidsPlaying;

	private int queueCounter = 3;
	private final IVisitRepository visitRepository;

	public Playsite(IVisitRepository visitRepository)
	{
		this.visitRepository = visitRepository;
	}

	@Override
	public void setPlaysiteType(PlaysiteType playsiteType)
	{
		this.playsiteType = playsiteType;
		maxKidsPlaying = playsiteType.getMaxKidsPlaying();
	}

	@Override
	public boolean addKid(Kid kid)
	{
		if (kidsPlaying.contains(kid) || vipKidsWaiting.contains(kid) || kidsWaiting.contains(kid))
		{
			throw new RuntimeException("Kid has " + kid + " already existed in playsite: " + this);
		}
		if (kidsPlaying.size() >= maxKidsPlaying)
		{
			switch (kid.getKidType())
			{
				case VIP -> vipKidsWaiting.add(kid);
				default -> kidsWaiting.add(kid);
			}

			return false;
		}
		else
		{
			kidsPlaying.add(kid);

			Visit visit = new Visit(this, kid, new Date(), true);
			visitRepository.save(visit);

			return true;
		}
	}

	@Override
	public void removeKid(Kid kid)
	{
		if (kidsWaiting.remove(kid))
		{
			return;
		}

		if (vipKidsWaiting.remove(kid))
		{
			return;
		}

		if (kidsPlaying.remove(kid))
		{
			Visit visit = new Visit(this, kid, new Date(), false);
			visitRepository.save(visit);

			if (queueCounter == 3 && !vipKidsWaiting.isEmpty())
			{
				Kid vipKid = vipKidsWaiting.remove(0);

				visit = new Visit(this, kid, new Date(), true);
				visitRepository.save(visit);

				queueCounter = 0;

				kidsPlaying.add(vipKid);
			}
			else if (queueCounter == 3 && vipKidsWaiting.isEmpty() && !kidsWaiting.isEmpty())
			{
				Kid movedKid = kidsWaiting.remove(0);

				visit = new Visit(this, movedKid, new Date(), true);
				visitRepository.save(visit);

				kidsPlaying.add(movedKid);
			}
			else if (queueCounter != 3 && !kidsWaiting.isEmpty())
			{
				Kid movedKid = kidsWaiting.remove(0);

				visit = new Visit(this, kid, new Date(), true);
				visitRepository.save(visit);

				queueCounter++;

				kidsPlaying.add(movedKid);
			}
		}
		else
		{
			throw new RuntimeException("No kid " + kid + " in playsite: " + this);
		}
	}

	@Override
	public List<Kid> getKidsPlaying()
	{
		return ImmutableList.copyOf(kidsPlaying);
	}

	@Override
	public List<Kid> getKidsWaiting()
	{
		return ImmutableList.<Kid>builder().addAll(kidsWaiting).addAll(vipKidsWaiting).build();
	}
}
