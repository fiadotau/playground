package com.swedbank.playground.domain.impl.filter;

import com.swedbank.playground.domain.IPlaysite;
import com.swedbank.playground.domain.impl.kid.Kid;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class VisitFilter
{
	IPlaysite playsite;
	Kid kid;
	boolean isStart;
}
