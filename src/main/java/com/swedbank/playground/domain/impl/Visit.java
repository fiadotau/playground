package com.swedbank.playground.domain.impl;

import com.swedbank.playground.domain.IPlaysite;
import com.swedbank.playground.domain.impl.kid.Kid;
import lombok.*;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
public final class Visit
{
	private IPlaysite playsite;
	private Kid kid;
	private Date time;
	private boolean isStart;  // true - start of visit; false - end of visit
}
