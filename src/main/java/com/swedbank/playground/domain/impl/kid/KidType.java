package com.swedbank.playground.domain.impl.kid;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum KidType
{
	VIP,
	GENERAL
}
