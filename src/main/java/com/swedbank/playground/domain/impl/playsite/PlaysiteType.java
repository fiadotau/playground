package com.swedbank.playground.domain.impl.playsite;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum PlaysiteType
{

	CAROUSEL(0),
	SLIDE(0),
	BALL_PIT(0),
	DOUBLE_SWINGS(2);

	private final int maxKidsPlaying;
}
