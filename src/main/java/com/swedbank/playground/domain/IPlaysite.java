package com.swedbank.playground.domain;

import com.swedbank.playground.domain.impl.playsite.PlaysiteType;
import com.swedbank.playground.domain.impl.kid.Kid;

import java.util.List;

public interface IPlaysite
{
	PlaysiteType getPlaysiteType();

	void setPlaysiteType(PlaysiteType playsiteType);

	boolean addKid(Kid kid);

	void removeKid(Kid kid);

	List<Kid> getKidsPlaying();

	List<Kid> getKidsWaiting();

	int getMaxKidsPlaying();

	void setMaxKidsPlaying(int maxKidsPlaying);
}
