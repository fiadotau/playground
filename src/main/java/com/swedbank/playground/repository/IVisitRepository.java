package com.swedbank.playground.repository;

import com.swedbank.playground.domain.impl.Visit;
import com.swedbank.playground.domain.impl.filter.VisitFilter;

import java.util.List;

public interface IVisitRepository
{
  void save(Visit visit);

  List<Visit> getByFilter(VisitFilter visitFilter);
}
